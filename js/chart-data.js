Chart.defaults.global.legend.display = false;

/** SYNTHESE**/

var ctx = document.getElementById("synthese").getContext("2d");
var data = {
  labels: ["National", "Region"],
  datasets: [{
        label: "Non classe",
        backgroundColor: "#fff",
        borderColor: "#ccc",
        borderWidth: 1,
        data: [5,5]
    }, {
      label: "Bronze",
        backgroundColor: "#d6744c",
        borderColor: "#d6744c",
        data: [45,20]
    }, {
        label: "Argent",
        backgroundColor: "#cccccc",
        borderColor: "#cccccc",
        data: [18,55]
    }, {
        label: "Or",
        backgroundColor: "#d7a73f",
        borderColor: "#d7a73f",
        data: [32,20]
    }]
};

var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    scales: {
      xAxes: [{
        stacked: true,
        display: true
      }],
      yAxes: [{
          stacked: true,
          ticks: {
            display: true
          },
          display: false
        }]
    }
  }
});


var barChart = {
    scales: {
      yAxes: [{
          ticks: {
            display: false,
            beginAtZero: true,
            steps: 11,
            max: 110
          },
      display: false,
      gridLines: {
                  color: "rgba(0, 0, 0, 0)",
              }
        }]
    },
    events: false,
    tooltips: {
        enabled: false
    },
    hover: {
        animationDuration: 0
    },
    animation: {
        duration: 1,
        onComplete: function () {
            var chartInstance = this.chart,
            ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = "#333";

            this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function (bar, index) {
                    var data = dataset.data[index];                            
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                });
            });
        }
    }
}

/** Global Score **/
var ctx = document.getElementById("globalScore").getContext("2d");
var data2 = {
    labels: ["Free-flow", "Caisse", "Plat Chaud", "Legumes", "Salle"],
    datasets: [
        {
            label: "My First dataset",
            backgroundColor: [
                'rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)'
            ],
            borderColor: [
                'rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)'
            ],
            borderWidth: 1,
            data: [65, 59, 80, 90, 67],
        }
    ],

};

var globalScore = new Chart(ctx, {
    type: 'bar',
    data: data2,
    options: barChart
});

/** Global Score Focus**/
var ctx = document.getElementById("globalScoreFocus").getContext("2d");
var data3 = {
    labels: ["Famille Enfant", "Anniversaire", "Flunch Cafe", "Flunch Traiteur", "Click & Collect"],
    datasets: [
        {
            label: "My First dataset",
            backgroundColor: [
                'rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)'
            ],
            borderColor: [
                'rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)','rgba(83, 209, 197, 1)'
            ],
            borderWidth: 1,
            data: [65, 59, 80, 90, 67],
        }
    ],

};

var globalScoreFocus = new Chart(ctx, {
    type: 'bar',
    data: data3,
    options: barChart
});